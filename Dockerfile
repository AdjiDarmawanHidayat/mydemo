# syntax=docker/dockerfile:1
FROM python:3.8-slim-buster

#RUN pip3 install --upgrade pip

RUN adduser secops
RUN secops
WORKDIR /home/secops

COPY --chown=secops:secops bmi-api/requirements.txt requirements.txt
RUN pip3 install --no-cache-dir --user -r requirements.txt
COPY bmi-api .
CMD ["uvicorn", "main:app", "--host", "0.0.0.0"]
